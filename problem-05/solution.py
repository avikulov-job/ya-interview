#!usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from collections import Counter


def is_anagram(s1:str, s2:str):
    cs1 = Counter(s1)
    cs2 = Counter(s2)

    if len(cs1.keys()) != len(cs2.keys()):
        return 0

    for (key, value) in cs1.items():
        if cs2.get(key, 0) != value:
            return 0

    return 1


if __name__ == '__main__':
    s1 = sys.stdin.readline().strip()
    s2 = sys.stdin.readline().strip()

    result = is_anagram(s1, s2)
    print(result)
