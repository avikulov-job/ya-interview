#!usr/bin/env python3
# -*- coding: utf-8 -*-

import sys


def count_treasures(treasures:str, stones:str):
    res = 0

    treasures_set = set(treasures)
    for s in stones:
        if s in treasures_set:
            res += 1

    return res


if __name__ == '__main__':
    j = sys.stdin.readline().strip()
    s = sys.stdin.readline().strip()

    result = count_treasures(treasures=j, stones=s)
    print(result)
