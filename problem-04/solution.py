#!usr/bin/env python3
# -*- coding: utf-8 -*-

import sys


# Solution: https://www.youtube.com/watch?v=zU-LndSG5RE
def print_parentheses(current:str, opened:int, closed:int, n:int):
    if len(current) == 2*n:
        print(current)
        return current

    res = ''
    if opened < n:
        res = print_parentheses(
            current=f'{current}(',
            opened=(opened + 1),
            closed=closed,
            n=n)

    if closed < opened:
        res = print_parentheses(
            current=f'{current})',
            opened=opened,
            closed=(closed + 1),
            n=n)

    return res


def generate(n:int):
    res = print_parentheses(current='', opened=0, closed=0, n=n)
    return res


if __name__ == '__main__':
    n = int(sys.stdin.readline().strip())
    result = generate(n=n)
