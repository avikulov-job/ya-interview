#!usr/bin/env python3
# -*- coding: utf-8 -*-

import sys


def get_unique_values(n:int):
    n = min(n, 10**6)
    last = float('-inf')

    for i in range(n):
        cur_int = int(sys.stdin.readline().strip())
        if last < cur_int:
            last = cur_int
            print(last)

    return last


if __name__ == '__main__':
    n = int(sys.stdin.readline().strip())
    result = get_unique_values(n=n)
