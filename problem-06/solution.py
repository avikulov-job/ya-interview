#!usr/bin/env python3
# -*- coding: utf-8 -*-

import sys


ALL_POSSIBLE_VALUES = [str(i) for i in range(101)]


def create_dict_with_possible_values():
    res = {i: 0 for i in ALL_POSSIBLE_VALUES}
    return res


def main():
    # ready code
    k = min(int(sys.stdin.readline().strip()), 1024)

    result = create_dict_with_possible_values()
    for i in range(k):
        for j in sys.stdin.readline().strip().split()[1:]:
            result[j] += 1

    for i in ALL_POSSIBLE_VALUES:
        # s = ('%s ' % i) * result[i]
        s = f'{i} ' * result[i]
        print(s, end='')

    return result


if __name__ == '__main__':
    main()
