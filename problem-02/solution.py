#!usr/bin/env python3
# -*- coding: utf-8 -*-

import sys


def count_ones(n:int):
    n = min(n, 10**4)

    current = 0
    best = 0

    for i in range(n):
        cur_int = int(sys.stdin.readline().strip())
        if cur_int == 1:
            current += 1
            best = max(best, current)

        else:
            current = 0

    return best


if __name__ == '__main__':
    n = int(sys.stdin.readline().strip())

    result = count_ones(n=n)
    print(result)
